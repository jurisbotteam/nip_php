<?php
include "Conn.php";

$protocolo = '';
if (isset($_GET['protocolo']))
{
   if(!empty($_GET['protocolo']))
   {
     $protocolo = $_GET["protocolo"];
   }
}

$demanda = '';
if (isset($_GET['demanda']))
{
   if(!empty($_GET['demanda']))
   {
     $demanda = $_GET["demanda"];
   }
}

$cpf = '';
if (isset($_GET['cpf']))
{
   if(!empty($_GET['cpf']))
   {
     $cpf = str_replace(".", "", str_replace("-", "",$_GET['cpf']));
   }
}

$token = $_GET['token'];

if($token == 'ProNipAns'){
    $conn = new Conn();
    $sql = 'select * from robos.ANS_Retorno 
    where ';
    
    $where = '';
    if($protocolo!='')
    {   
        $where = $where . ' N_protocolo = ' . $protocolo;
    }
    if($demanda!='')
    {   
        if($where!=''){
            $where = $where . ' or ';
        }
        $where = $where . ' N_demanda = ' . $demanda;
    }
    if($cpf!='')
    {   
        if($where!=''){
            $where = $where . ' or ';
        }
        $where = $where . ' Cpf_Beneficiario = ' . $cpf;
    }
    
    
    $sql = $sql . $where . ';';

    $db = $conn::getConn();        
    $retTokens = $db->prepare($sql);
    $retTokens->execute();
    $linha = $retTokens->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($linha);
}

